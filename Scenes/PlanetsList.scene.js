import React, { useState, useEffect } from 'react';
import { View } from 'react-native';
import { Button, Text, List, Container } from 'native-base';

import axios from 'axios';
import Card from '../components/Card.component';
import Loading from '../components/Loading.component';


export default function PlanetsList (props) {
    const {navigation} = props;
    const [planets, setPlanets] = useState(null);
    const [page, setPage] = useState(1);
    const [total, setTotal] = useState(0);
    const api = axios.create({
        baseURL: 'https://swapi.dev/api/',
    });

    useEffect(() => {
        api.get(`planets/`)
            .then(function (response) {
                setTotal(response.data.results.length);
                setPlanets(response.data)
            })
            .catch(function (error) {
                alert("Ocorreu um erro");
            });
        return function cleanup () {
            // caso necessário aqui seria um local para descontruir a lista
        };
    }, []);

    function getPage (type) {
        setPlanets(null);
        axios.get(type === "previous" ? planets.previous : planets.next)
            .then(function (response) {
                type === "previous" ? setPage(page - 1) : setPage(page + 1);
                setPlanets(response.data);
            })
            .catch(function (error) {
                alert("Ocorreu um erro");
            });
    }
    function RenderList () {
        return (
            <div>
                <List>
                    {planets.results.map((planet, index) => {
                        return (
                            <Card key={index} planet={planet} {...props} />
                        )
                    })}
                </List>
                <View style={{
                    flex: 1,
                    flexDirection: 'row',
                    justifyContent: 'space-around',
                    alignItems: 'center',
                    width: '100%',
                    padding: 10,
                }}>
                    <Button
                        onPress={() => getPage('previous')}
                        disabled={!planets.previous}
                        transparent
                    >
                        <Text>Anterior</Text>
                    </Button>
                    <Text> {`${total * page}/${(planets.count)}`} </Text>
                    <Button
                        onPress={() => getPage('next')}
                        disabled={!planets.next}
                        transparent
                    >
                        <Text>Próximo</Text>
                    </Button>
                </View>
            </div>
        )
    }
    return (
        <Container>
            {planets
                ? <RenderList />
                : <Loading />
            }
        </Container>
    );
}

