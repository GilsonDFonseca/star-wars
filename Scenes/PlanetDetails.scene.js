import React, { useState } from 'react';
import { StyleSheet, View, ScrollView } from 'react-native';
import { Text, List, ListItem, Container } from 'native-base';
import { numberMask } from '../Utils/Utils';

import axios from 'axios';
import Loading from '../components/Loading.component';


export default function PlanetsList (props) {
    const planet = props.route.params.planet;
    const [residents, setResidents] = useState([]);

    function getResidents () {
        try {
            if (residents.length === 0 && planet.residents.length !== 0) {
                console.log(residents)
                axios.all(planet.residents.map(resident => axios.get(resident)))
                    .then(
                        axios.spread((...responses) => {
                            let residentArray = responses.concat(responses);
                            if (residentArray.length !== 0) {
                                setResidents(residentArray)
                            }
                            else {

                            }
                        })
                    )
            }
        } catch {
            console.log("Ocorreu um erro")
        }
    }


    function RenderDetails () {
        getResidents()
        return (
            <View style={{
                flex: 1,
                flexDirection: 'column',
                alignItems: 'center',
                width: '100%',
                paddingTop: 10,
            }}>
                <Text style={styles.title}>Planeta: {planet.name}</Text>
                <Text style={styles.subtitle}>Clima: {planet.climate}</Text>
                <Text style={styles.subtitle}>Terreno: {planet.terrain}</Text>
                <Text style={styles.subtitle}>População: {numberMask(planet.population)}</Text>
                <Text style={styles.title}>{'Residentes'}</Text>
                <List>
                    {residents.length !== 0
                        ? residents.map(resident => {
                            return (
                                <ScrollView >
                                    <View style={styles.pair}>
                                        <Text style={styles.nameText}>Nome:  {resident.data.name}</Text>
                                        <Text style={styles.massText}>Massa:  {resident.data.mass === "unknown" ? 0 : resident.data.mass}</Text>
                                    </View>
                                </ScrollView>
                            )
                        })
                        : planet.residents.length === 0
                            ? <Text style={styles.massText}>Não existem residentes deste planeta</Text>
                            : <Loading />}
                </List>
            </View>
        )
    }
    return (
        <Container>
            {residents
                ? <RenderDetails />
                    : <Loading />
            }
        </Container>
    );
}


const styles = StyleSheet.create({
    pair: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    title: {
        fontSize: '36px'
    },
    subtitle: {
        fontSize: '18px'
    },
    nameText: {
        overflow: 'hidden',
        textOverflow: 'ellipsis',
        whiteSpace: 'nowrap',
        width: '60%',
    },
    massText: {
        // overflow: 'hidden',
        // textOverflow: 'ellipsis',
        // whiteSpace: 'nowrap',
    }
});
