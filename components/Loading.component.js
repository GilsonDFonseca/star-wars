import React, { Component } from 'react';
import { Spinner } from 'native-base';
import { View } from 'react-native';

export default class SpinnerExample extends Component {
    render () {
        return (
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                <Spinner color='black' />
            </View>
        );
    }
}