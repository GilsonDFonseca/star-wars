import React, { Component } from 'react';
import { TouchableOpacity } from 'react-native';
import { View, Card, CardItem, Text, Body } from 'native-base';
import { numberMask } from '../Utils/Utils';

export default function PlanetCard (props ) {
    const { planet, navigation } = props;

    return (
        <TouchableOpacity onPress={() => navigation.navigate('Detalhes', {planet})}>
            <View style={{
                width: '100%',
                padding: 10,
                flexDirection: "row",
                justifyContent: "center",
                alignItems: "center",
                backgroundColor: "#cdcdcd",

            }}>
                <Card style={{
                    width: '100%',
                    minHeight: 200,
                    flexDirection: "column",
                    justifyContent: "center",
                    alignItems: "center",
                }} >
                    <CardItem header >
                        <Text style={{
                            textShadow: "2px 2px #dddddd",
                            fontSize: '24px'
                        }}>{planet.name}</Text>
                    </CardItem>
                    <CardItem >
                        <Body>
                            <Text style={{ textShadow: "2px 2px #dddddd" }}>
                                População: {numberMask(planet.population)}
                            </Text>
                        </Body>
                    </CardItem>
                </Card>
            </View>
        </TouchableOpacity>

    );

}