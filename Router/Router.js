import * as React from 'react';
import { View, Text, Button } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import PlanetsList from '../Scenes/PlanetsList.scene';
import PlanetsDetails from '../Scenes/PlanetDetails.scene';

const Stack = createStackNavigator();

function App () {
    return (
        <NavigationContainer>
            <Stack.Navigator initialRouteName="Home">
                <Stack.Screen name="Planetas">
                    {props => <PlanetsList {...props}/>}
                </Stack.Screen>
                <Stack.Screen name="Detalhes">
                    {props => <PlanetsDetails {...props}/>}
                </Stack.Screen>
            </Stack.Navigator>
        </NavigationContainer>
    );
}

export default App;
