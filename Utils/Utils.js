export function numberMask (number) {
    var formatted = 0;
    if (number) {
        number = number.toString();
        var invertedArray = number.split("").reverse().join("");

        var splitedArray = [];
        var i = 0;
        for (i = 0; i < number.length; i = i + 3)
            splitedArray.push(invertedArray.substring(i, i + 3).split("").reverse().join(""))
        for (i = splitedArray.length - 1; i >= 0; i--) {
            formatted += splitedArray[i] + ".";
        }
        formatted = formatted.substring(1, formatted.length);
        formatted = formatted.substring(0, formatted.length - 1);
    }
    return formatted;
}